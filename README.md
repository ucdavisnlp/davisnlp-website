# DavisNLP Website

This is the source for the Davis NLP Group website. It is built on ReactJS + TypeScript using the React Bootstrap UI Library.

## Instructions

Please submit an issue to the project for any issues you have found. This includes:

- Typo, Grammatical errors
- UI/UX issues / suggestions
- Name misspelling, change of profile pictures
- Updates to publications, source codes, projects
- Updates to feature highlights
- Suggestions for added features.

You can view the current roadmap on the [[ Trello board ]](https://trello.com/invite/b/crMdGAEF/cc9388fce11d917f1df0f3932b021f7d/davisnlp-website).

## Usage

### Dependencies

You will need NodeJS and NPM to use this project. See `package.json` for dependencies and versions requirement.

Run `npm install` to install all necessary dependencies.

### Running the Project

To run the webpage locally, use `npm start`.

You can build a static version using `npm run build`, you will need the `serve` package to serve the webpage locally.

See `package.json` for the available npm scripts.

### Deployment

The project is set up to be deployed using GitLab Pages automatically on the `master` branch. See `.gitlab-ci.yml` for CI deployment configurations.

## Contributions

You can submit a PR directly on the project. Or contact the project maintainer directly for more info. Please fork from the `develop` branch.

## For Content Editors

### Data

Currently most data is embedded within the source code. Most data, such as publications, people, are embedded in a `.json` file next to the corresponding page source code. For example, the `peopledata.json` is located next to the `people.tsx` file.

Reference the source code where the json file is used for the data structure. You can also reference the json itself to see how the data should be structured.

### Image

Images are stored in the `public/` directory, under the corresponding folders.
