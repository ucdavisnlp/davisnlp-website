import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export const Footer: React.FC = () => {
	return (
		<div className="Footer">
			<Container>
				<Row>
					<Col xs={6} sm={6} md={3}>
						<h1>UC Davis NLP Group</h1>
						<ul>
							<li>Academic Surge 2251 <br />
								1 Shields Ave <br />
								Davis, CA 95616 <br />
							</li>
							<li>
								<a
									href="https://goo.gl/maps/MFaM9QviuuJ7aGnVA"
									target="_blank"
									rel="noopener noreferrer"
								> Directions and Parking </a>
							</li>
						</ul>
					</Col>

					<Col xs={6} sm={6} md={3}>
						<h1>Affiliated Groups</h1>
						<ul>
							<li><a
								href="https://it2003.github.io/UCD-ML/"
								target="_blank"
								rel="noopener noreferrer"
							>
								UC Davis ML and AI Group
								</a></li>
						</ul>
					</Col>

					<Col xs={6} sm={6} md={3}>
						<h1>Connect</h1>
						<ul>
							<li>
								Twitter: <a
									href="https://twitter.com/Zhou_Yu_AI"
									target="_blank"
									rel="noopener noreferrer"
								> @Zhou_Yu_AI </a>
							</li>
							<li>
								Twitter: <a
									href="https://twitter.com/tgunrock"
									target="_blank"
									rel="noopener noreferrer"
								> @TGunrock </a>
							</li>
						</ul>
					</Col>

					<Col xs={6} sm={6} md={3}>
						<h1>Local Links</h1>
						<ul>
							<li>
								<a
									href="https://www.cs.ucdavis.edu/"
									target="_blank"
									rel="noopener noreferrer"
								> UC Davis CS Department </a>
							</li>
						</ul>
					</Col>
				</Row>
			</Container>
		</div>
	)
}