import React from 'react';
import { Link } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import 'bootstrap/dist/css/bootstrap.min.css';


export const Header: React.FC = () => {
	return (
		<Navbar bg="light" expand="sm">
			<Container>
				<Navbar.Brand as={Link} to="/">DavisNLP</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link as={Link} to="/">Home</Nav.Link>
						<Nav.Link as={Link} to="/people">People</Nav.Link>
						<Nav.Link as={Link} to="/publications">Publications</Nav.Link>
						{/* <Nav.Link as={Link} to="/news">News</Nav.Link> */}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}
