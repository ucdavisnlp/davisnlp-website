import React from 'react';

import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

import ListGroup from 'react-bootstrap/ListGroup';

import { Page, PageHeader } from '../components';

import publicationsdata from './publicationsdata.json';

interface IPaper {
	title: string,
	author: string,
	venue: string,
	year?: number,
	extra_venue?: string,
	url: { "type": string, "url": string }[],
	remarks?: string,
}

export const Publications: React.FC = () => {
	return (
		<div>
			<PageHeader title="Publications" />
			<Page>
				<Tabs defaultActiveKey="paper" id="publication-tabs">
					<Tab eventKey="paper" title="Papers">
						<PublicationsList list={(publicationsdata as { "papers": IPaper[] }).papers} />
					</Tab>
				</Tabs>
			</Page>
		</div>
	)
}

const PublicationsList: React.FC<{ "list": IPaper[] }> = (props) => {
	return (
		<ListGroup>
			{props.list.map((item: IPaper) => <PublicationsListItem item={item} />)}
		</ListGroup>
	)
}

const PublicationsListItem: React.FC<{ "item": IPaper }> = (props) => {
	return (
		<ListGroup.Item>
			<div className="p_grouped">
				<p>{[props.item.author, [props.item.venue, props.item.year, props.item.extra_venue].filter(Boolean).join(' '), ""].join('. ')}</p>
				<p><b>{props.item.title}</b></p>
				<p>{props.item.url.map(({ type, url }) => (
					<span>[ <a href={url} target="_blank" rel='noreferrer noopener'>{type}</a> ]</span>
				))}</p>
				{props.item.remarks ? <mark>{props.item.remarks}</mark> : null}
			</div>
		</ListGroup.Item>
	)
}

// const PublicationsList: React.FC<{"list": IPublication[]}> = (props) => {
//   return (
//     <List 
//       dataSource={props.list}
//       renderItem={(item: IPublication) => (
//         <List.Item>
//           <div className="p_grouped" style={{padding: "0 1em"}}>
//             <p>{[item.author, [item.venue, item.year, item.extra_venue].filter(Boolean).join(' '), ""].join('. ')}</p>
//             <p><b>{item.title}</b></p>
//             <p>{item.url.map(({type, url}) => (
//               <span>[ <a href={url} target="_blank" rel='noreferrer noopener'>{type}</a> ]</span>
//             ))}</p>
//             {item.remarks ? <Text mark>{item.remarks}</Text> : null}
//           </div>
//         </List.Item>
//       )}
//     />
//   )
// }