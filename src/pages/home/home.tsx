import React from 'react';

import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import Image from 'react-bootstrap/Image';
import Carousel from 'react-bootstrap/Carousel';

import { Timeline } from 'react-twitter-widgets';

import { Page, PageHeader } from '../components';

import researchhighlightdata from './researchhighlight.json';


interface IResearchHighlight {
	title: string,
	description: string,
	image: string,
	image_alt: string,
	link: string,
}

export const Home: React.FC = () => {
	return (
		<div>
			<PageHeader title="UC Davis NLP Group" />
			<Page>
				<Row>
					<Col xs={12} sm={12} md={7} lg={7}>
						<Welcome />
					</Col>
					<Col xs={12} sm={12} md={5} lg={5}>
						<ResearchHighlight list={researchhighlightdata} />
						<RecentActivity />
					</Col>
				</Row>
			</Page>
		</div>
	)
}

const Welcome: React.FC = () => {
	return (
		<div className="Page-Paragraph" >
			<h1>Welcome</h1>
			<p>The goal of the UC Davis Natural Language Processing Group is to develop algorithms and theories that enable computers to learn human languages. Our team includes faculty, post-docs, students, and engineers from across multiple departments, including from Computer Science, Linguistics, and Computer Engineering. Our research spans <b>natural language understanding</b>, <b>semantic analysis</b>, <b>language generation</b>, <b>visual grounding</b>, and <b>intelligent dialog systems</b>. We are additionally interested in utilizing natural language processing for important social applications.</p>
			<p> The central research topic for our research group revolves around interactive dialog systems.  Our dialog system won <b>2018 Amazon Alexa Prize Social-bot Challenge</b>.</p>
			<p>Our current research interests include:</p>
			<ul>
				<li>Interactive Dialog Systems</li>
				<li>Real-Time Intelligent Interactive Systems</li>
				<li>Multimodal Sensing and Analysis</li>
				<li>Machine Learning</li>
				<li>Human Computer Interaction</li>
			</ul>
			<Image src={`${process.env.PUBLIC_URL}/images/group_main_1440.jpg`} fluid />
		</div>
	)
}


const ResearchHighlight: React.FC<{ "list": IResearchHighlight[] }> = (props) => {
	return (
		<div className="Page-Paragraph">
			<h1>Highlights</h1>
			<Carousel>
				{props.list.map((item: IResearchHighlight) => (
					<Carousel.Item>
						<a
							href={item.link}
							target="_blank"
							rel="noopener noreferrer"
						>
							<img
								className="d-block w-100"
								src={`${process.env.PUBLIC_URL}${item.image}`}
								alt={item.image_alt}
							/>
						</a>
						<Carousel.Caption style={{
							"backgroundColor": "rgba(0, 40, 85, 0.75)",
							"right": "0%",
							"left": "0%",
							"padding": "0.25rem 5%"
						}}>
							<h3 style={{ "fontSize": "1rem" }}>{item.title}</h3>
							{/* <p>{item.description}</p> */}
						</Carousel.Caption>
					</Carousel.Item>
				))}
			</Carousel>
		</div>
	)
}

const RecentActivity: React.FC = () => {
	return (
		<div className="Page-Paragraph">
			<h1>News</h1>
			<Timeline
				dataSource={{
					sourceType: 'profile',
					screenName: 'Zhou_Yu_AI'
				}}
				options={{
					height: '500',
					chrome: "noheader"
				}}
			/>
		</div>
	)
}