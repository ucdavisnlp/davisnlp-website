import React from 'react';
import Container from 'react-bootstrap/Container';

export const PageHeader: React.FC<{ "title": string }> = (props) => (
	<div className="Page-Header">
		<Container>{props.title}</Container>
	</div>
)

export const Page: React.FC = (props) => (
	<div className="Page">
		<Container>
			{props.children}
		</Container>
	</div>
)

export const ALink: React.FC<{ "url": string }> = (props) => (
	<a
		href={props.url}
		target="_blank"
		rel="noopener noreferrer"
	>
		{props.children}
	</a>
)