import React, { Component } from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Card from 'react-bootstrap/Card';
import CardImg from 'react-bootstrap/CardImg'

import { Page, PageHeader, ALink } from '../components';

import peopledata from './peopledata.json';


interface IPeopleList {
	category: string,
	grid?: {
		"xs": number,
		"sm": number,
		"md": number,
		"lg": number,
		"xl": number,
	}
	size?: string,
	people: IPerson[],
}

interface IPerson {
	name: string;
	dept?: string;
	portraiturl?: string;
	title?: string;
	bio?: string;
	url?: string;
}

export const People: React.FC = () => (
	<div>
		<PageHeader title="People" />
		<Page>
			{peopledata.map((peoplelist) =>
				<div key={peoplelist.category}>
					<h2>{peoplelist.category}</h2>
					<PeopleList peoplelist={peoplelist} />
				</div>
			)}
		</Page>
	</div>
)


class PeopleList extends Component<{ "peoplelist": IPeopleList }> {

	render() {
		const grid = this.props.peoplelist.grid ? this.props.peoplelist.grid : {
			"xs": 6, "sm": 4, "md": 4, "lg": 3, "xl": 2
		}
		const peoplelist = this.props.peoplelist.people.sort((a, b) => {
			const a_key = a.name.split(" ").pop() as string
			const b_key = b.name.split(" ").pop() as string
			if (a_key < b_key) { return -1 }
			else if (a_key > b_key) { return 1 }
			else { return 0 }
		})
		const gutter = "0.25rem"
		return (
			<Row noGutters style={{ margin: `-${gutter}` }}>
				{peoplelist.map((person) => (
					<Col {...grid} key={person.name}>
						<PeopleCard person={person} gutter={gutter} />
					</Col>
				))}
			</Row>
		)
	}
}

class PeopleCard extends Component<{ "person": IPerson, "gutter": string }> {

	cardImage: React.FC<{ "portraiturl"?: string }> = (props) => (
		<CardImg src={(() => {
			if (!props.portraiturl) {
				return `${process.env.PUBLIC_URL}/images/people_icon.svg`
			} else if (props.portraiturl.startsWith("/")) {
				return `${process.env.PUBLIC_URL}${props.portraiturl}`
			} else {
				return props.portraiturl
			}
		})()} />
	)

	cardDefault: React.FC<{"person": IPerson}> = (props) => (
		<Card style={{ margin: this.props.gutter }}>
			<ALink
				url={props.person.url ? props.person.url :
					`https://directory.ucdavis.edu/search/directory_results.shtml?filter=${encodeURIComponent(this.props.person.name)}`
				}
			>
				<this.cardImage portraiturl={props.person.portraiturl} />
			</ALink>
			<Card.Body>
				<Card.Title>{props.person.name}</Card.Title>
				<Card.Text>{props.person.title ? props.person.title : props.person.dept}</Card.Text>
			</Card.Body>
		</Card >
	)

	cardImgLeft: React.FC<{"person": IPerson}> = (props) => (
		<Card style={{ margin: this.props.gutter }}>
			<Row>

			</Row>
			<ALink
				url={props.person.url ? props.person.url :
					`https://directory.ucdavis.edu/search/directory_results.shtml?filter=${encodeURIComponent(this.props.person.name)}`
				}
			>
				<this.cardImage portraiturl={props.person.portraiturl} />
			</ALink>
			<Card.Body>
				<Card.Title>{props.person.name}</Card.Title>
				<Card.Text>{props.person.title ? props.person.title : props.person.dept}</Card.Text>
			</Card.Body>
		</Card >
	)

	render() {
		return <this.cardDefault person={this.props.person} />
	}
}