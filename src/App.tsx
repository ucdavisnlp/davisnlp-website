import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';

import { Header, Footer } from './common';
import { Home, People, Publications } from './pages';

const App: React.FC = () => {
	return (
		<Router basename={process.env.PUBLIC_URL}>
			<Header />
			<Route exact path="/" component={Home} />
			<Route path="/people" component={People} />
			<Route path="/publications" component={Publications} />
			<Footer />
		</Router>
	);
}

export default App;
